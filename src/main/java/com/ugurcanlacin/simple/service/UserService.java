package com.ugurcanlacin.simple.service;

import com.ugurcanlacin.simple.model.User;

import java.util.List;

public interface UserService {
	List<User> findAll();
	void persistUser(User user);
	User findUserById(int id);
	void updateUser(User user);
	void deleteUser(User user);
}
