package com.ugurcanlacin.simple.dao;

import com.ugurcanlacin.simple.model.User;

import java.util.List;

public interface UserDao {
	List<User> findAll();
	void persistUser(User user);
	User findUserById(int id);
	void updateUser(User user);
	void deleteUser(User user);
}
