package com.ugurcanlacin.simple.dao.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ugurcanlacin.simple.dao.UserDao;
import com.ugurcanlacin.simple.model.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("userDao")
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<User> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from User");
        List<User> users = query.list();
        return users;
    }

    public void persistUser(User user) {
        sessionFactory.getCurrentSession().persist(user);
    }

    public User findUserById(int id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }

    public void updateUser(User user) {
        sessionFactory.getCurrentSession().update(user);

    }

    public void deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);

    }

}
